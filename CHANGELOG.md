# Change log

## unreleased

* Capture screenshot every two seconds
* Closing the windows with "Escape" and "Ctrl+Q"
* Choose filter from dropdown menu
* Zoom into screenshot
* Add MIT license
