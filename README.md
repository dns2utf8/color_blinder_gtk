# color_blinder_gtk

An interactive application allowing you to simulate 11 different kinds of color blindness on your screen in real time.

This application is based upon [the cli tool color_blinder](https://gitlab.com/dns2utf8/color_blinder).

![screenshot](public/screenshot.png)

## Features

* [x] Grab different windows
    * [x] Decode latin-1 window titles
* [x] Use all filters
    * [ ] Expose group filters
* [ ] Add Pause button
* [ ] Add Save to file button

## Installation

### With cargo (or rustup) from git

```bash
sudo apt install build-essential libxrandr-dev libpango1.0-dev libatk1.0-dev libgtk-3-dev
cargo install --branch main --git https://gitlab.com/dns2utf8/color_blinder_gtk.git
```

### Download nightly from gitlab:

The CI builds the binary on the `main` branch on every commit.
You can download the program from [https://dns2utf8.gitlab.io/color_blinder_gtk/color_blinder_gtk](https://dns2utf8.gitlab.io/color_blinder_gtk/color_blinder_gtk) or use these commands:

```bash
sudo mkdir /usr/local/bin/
sudo wget -O /usr/local/bin/color_blinder_gtk https://dns2utf8.gitlab.io/color_blinder_gtk/color_blinder_gtk
sudo chmod +x /usr/local/bin/color_blinder_gtk
```

**Note:** You may need to install the **gtk3 runtime** by yourself.

### Install APT package

TODO contributions are welcome!
